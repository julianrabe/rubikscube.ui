package de.uniks.rubikscube.ui;

import de.uniks.rubikscube.model.Cube;
import de.uniks.rubikscube.model.CubeSide;
import de.uniks.rubikscube.model.CubeSideIdentifier;
import de.uniks.rubikscube.model.XY;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

import java.util.Collection;
import java.util.HashSet;

public class CubeImageController {

    private final Cube cube;
    private final ImageView imageView;
    private final HashSet<FaceletController> faceletControllers;

    private Label probeStatusLabel;
    private SimpleImageUI image;
    private CubeSideIdentifier probeConfigureMode;
    private int probeConfigureCounter;

    CubeImageController(Cube cube,
                        Collection<CubeSide> attachedCubeSides,
                        ImageView imageView,
                        StackPane imageContainer,
                        Pane markerPane,
                        CubeUI cubeUI,
                        SimpleImageUI image) {
        this(cube, attachedCubeSides, imageView, imageContainer, markerPane, cubeUI);
        this.image = image;

        imageView.setImage(image);
    }

    CubeImageController(Cube cube,
                        Collection<CubeSide> attachedCubeSides,
                        ImageView imageView,
                        StackPane imageContainer,
                        Pane markerPane,
                        CubeUI cubeUI) {
        this.cube = cube;
        this.imageView = imageView;
        this.faceletControllers = new HashSet<>();

        imageView.fitWidthProperty().bind(imageContainer.widthProperty());
        imageView.fitHeightProperty().bind(imageContainer.heightProperty());

        imageView.boundsInParentProperty().addListener((observable) -> faceletControllers.forEach(FaceletController::rescaleProbeMarkers));

        for (CubeSide cubeSide : attachedCubeSides) {
            for (int i = 0; i < 9; i++) {
                faceletControllers.add(new FaceletController(this, markerPane,
                        cubeSide.getFacelet(i), cubeUI.getFacelet(cubeSide.getId(), i)));
            }
        }
    }

    public void setProbeStatusLabel(Label probeStatusLabel) {
        this.probeStatusLabel = probeStatusLabel;
    }

    /**
     * Starts the configuration for probe points.
     *
     * @param probeConfigureMode The CubeSide to configure.
     */
    public void setProbeConfigureMode(CubeSideIdentifier probeConfigureMode) {
        this.probeConfigureMode = probeConfigureMode;
        probeConfigureCounter = 0;

        if (probeStatusLabel != null) {
            probeStatusLabel.setText("Configuring " + probeConfigureMode.name() + ":" + 0 + "...");
        }
    }

    /**
     * Records new coordinates for probe points and writes them to the model.
     *
     * @param event The mouse event to get coordinates from.
     */
    public void recordProbePoint(MouseEvent event) {
        if (probeConfigureMode != null) {
            cube.getCubeSide(probeConfigureMode).getFacelet(probeConfigureCounter++).setImageCoordinates(
                    new XY((int) (event.getX() / computeScaleFactor()), (int) (event.getY() / computeScaleFactor())));

            String probeStatus = null;

            if (probeConfigureCounter >= 9) {
                probeConfigureMode = null;
            } else {
                probeStatus = "Configuring " + probeConfigureMode.name() + ":" + probeConfigureCounter + "...";
            }

            if (probeStatusLabel != null) {
                probeStatusLabel.setText(probeStatus);
            }
        }
    }

    /**
     * Computes scale factor to convert between UI and image pixels.
     * Used for window resizing as well.
     * <p>
     * TODO Is height important? (Probably not, because ImageViews maintain aspect ratio on resize)
     *
     * @return &lt; 1.0 when image is smaller than its current UI representation, &gt; 1.0 when its larger,
     * 1.0 when both are equal.
     */
    public double computeScaleFactor() {
        return imageView.getBoundsInParent().getWidth() / imageView.getImage().getWidth();
    }

    public SimpleImageUI getImage() {
        return image;
    }

    public void setImage(SimpleImageUI image) {
        this.image = image;
    }
}

package de.uniks.rubikscube.ui;

import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.util.ArrayList;

/**
 * Draws a cube side in cube's 2D representation.
 */
public class CubeSideUI extends AnchorPane {

    private ArrayList<Rectangle> facelets;

    CubeSideUI(double faceletSize) {

        facelets = new ArrayList<>(9);

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {

                Rectangle facelet = new Rectangle(j * faceletSize, i * faceletSize, faceletSize, faceletSize);
                facelet.setStroke(Paint.valueOf("black"));
                facelet.setFill(Paint.valueOf("white"));

                getChildren().addAll(facelet);
                facelets.add(facelet);
            }
        }
    }

    public Rectangle getFacelet(int index) {
        return facelets.get(index);
    }
}

package de.uniks.rubikscube.ui;

import de.uniks.rubikscube.model.CubeSideIdentifier;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;

import java.util.HashMap;

/**
 * Draws the cube's 2D representation.
 */
class CubeUI extends AnchorPane {

    private final double textHeight = 15d;

    private final double faceletSize;
    private HashMap<CubeSideIdentifier, CubeSideUI> cubeSides;

    CubeUI(double faceletSize) {
        this.faceletSize = faceletSize;
        cubeSides = new HashMap<>(6);

        // Top side
        createLabel("Top", null, null, faceletSize * 3, null);
        createCubeSide(CubeSideIdentifier.TOP, textHeight, null, faceletSize * 3, null);

        // Left side
        createLabel("Left", faceletSize * 3, null, null, null);
        createCubeSide(CubeSideIdentifier.LEFT, faceletSize * 3 + textHeight, null, null, null);

        // Front side
        createLabel("Front", faceletSize * 3, null, null, faceletSize * 9 + 1);
        createCubeSide(CubeSideIdentifier.FRONT, faceletSize * 3 + textHeight, null, faceletSize * 3, null);

        // Right side
        createLabel("Right", faceletSize * 3, null, faceletSize * 6, null);
        createCubeSide(CubeSideIdentifier.RIGHT, faceletSize * 3 + textHeight, null, faceletSize * 6, null);

        // Back side
        createLabel("Back", faceletSize * 3, null, faceletSize * 9, null);
        createCubeSide(CubeSideIdentifier.BACK, faceletSize * 3 + textHeight, null, faceletSize * 9, null);

        // Bottom side
        createLabel("Bottom", faceletSize * 9 + textHeight, null, faceletSize * 3, null);
        createCubeSide(CubeSideIdentifier.BOTTOM, faceletSize * 6 + textHeight, null, faceletSize * 3, null);
    }

    private void createCubeSide(CubeSideIdentifier id, Double top, Double bottom, Double left, Double right) {
        CubeSideUI cubeSide = new CubeSideUI(faceletSize);

        setTopAnchor(cubeSide, top);
        setBottomAnchor(cubeSide, bottom);
        setLeftAnchor(cubeSide, left);
        setRightAnchor(cubeSide, right);

        cubeSides.put(id, cubeSide);
        getChildren().add(cubeSide);
    }

    private void createLabel(String labelText, Double top, Double bottom, Double left, Double right) {
        Label label = new Label(labelText);

        setTopAnchor(label, top);
        setBottomAnchor(label, bottom);
        setLeftAnchor(label, left);
        setRightAnchor(label, right);

        label.setPrefHeight(textHeight);

        getChildren().add(label);
    }

    /**
     * Returns a single facelet's UI representation.
     *
     * @param cubeSideIdentifier The cube side to get the facelet from.
     * @param faceletId          The facelet id (0-8)
     * @return The JavaFX node which represents the facelet in the 2D drawing.
     */
    public Rectangle getFacelet(CubeSideIdentifier cubeSideIdentifier, int faceletId) {
        return cubeSides.get(cubeSideIdentifier).getFacelet(faceletId);
    }
}

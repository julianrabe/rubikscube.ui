package de.uniks.rubikscube.ui;

import de.uniks.rubikscube.model.Facelet;
import de.uniks.rubikscube.model.XY;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Rectangle;

import java.beans.PropertyChangeEvent;

/**
 * Used to draw and update probe markers and update colors in the UI.
 */
class FaceletController {

    private final CubeImageController cubeImageController;
    private final Pane markerPane;
    private final ProbeMarker probeMarker;

    FaceletController(CubeImageController cubeImageController, Pane markerPane, Facelet facelet, Rectangle faceletUi) {
        this.cubeImageController = cubeImageController;
        this.markerPane = markerPane;

        probeMarker = new ProbeMarker(new XY(0, 0), Paint.valueOf("cyan"));

        facelet.addPropertyChangeListener(Facelet.PROPERTY_IMAGE_COORDINATES, this::redrawMarker);
        facelet.addPropertyChangeListener(Facelet.PROPERTY_COLOR,
                event -> faceletUi.setFill(Paint.valueOf(facelet.getColor().getRange().name())));
    }

    /**
     * Redraws probe markers when {@link Facelet} coordinates change.
     *
     * @param event The {@link PropertyChangeEvent} to get the new and old values from.
     */
    private void redrawMarker(PropertyChangeEvent event) {
        if (event.getNewValue() == null) {
            markerPane.getChildren().remove(probeMarker);
        } else {
            probeMarker.setCoordinates((XY) event.getNewValue());
            probeMarker.redrawWithScale(cubeImageController.computeScaleFactor());
            if (event.getOldValue() == null) {
                markerPane.getChildren().add(probeMarker);
            }
        }
    }

    /**
     * Called to redraw probe markers (a.k.a. update their UI x/y values) when the windows is resized.
     */
    public void rescaleProbeMarkers() {
        probeMarker.redrawWithScale(cubeImageController.computeScaleFactor());
    }
}

package de.uniks.rubikscube.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String... args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResource("main.fxml").openStream());

        ((MainController) loader.getController()).setPrimaryStage(primaryStage);

        Scene scene = new Scene(root);

        primaryStage.setTitle("Rubik's Cube Solver");
        primaryStage.setMinHeight(800);
        primaryStage.setMinWidth(1000);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}

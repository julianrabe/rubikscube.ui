package de.uniks.rubikscube.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.uniks.rubikscube.model.*;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML MenuBar menuBar;
    @FXML ImageView leftImageView;
    @FXML ImageView rightImageView;
    @FXML StackPane leftImageContainer;
    @FXML StackPane rightImageContainer;
    @FXML Pane leftMarkerPane;
    @FXML Pane rightMarkerPane;
    @FXML AnchorPane recognizedCubeImage;
    @FXML Label probeStatus;
    @FXML MenuItem probeTop;
    @FXML MenuItem probeBottom;
    @FXML MenuItem probeLeft;
    @FXML MenuItem probeRight;
    @FXML MenuItem probeFront;
    @FXML MenuItem probeBack;
    @FXML MenuItem saveProbePoints;
    @FXML MenuItem openProbePoints;

    private Stage primaryStage;

    private SimpleImageUI leftImage;
    private SimpleImageUI rightImage;

    private CubeImageController leftImageController;
    private CubeImageController rightImageController;

    private Cube modelCube;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        menuBar.setUseSystemMenuBar(true);

        modelCube = new Cube();

        HashSet<CubeSide> leftCubeSides = new HashSet<>(3);
        leftCubeSides.add(modelCube.getCubeSide(CubeSideIdentifier.TOP));
        leftCubeSides.add(modelCube.getCubeSide(CubeSideIdentifier.LEFT));
        leftCubeSides.add(modelCube.getCubeSide(CubeSideIdentifier.FRONT));

        HashSet<CubeSide> rightCubeSides = new HashSet<>(3);
        rightCubeSides.add(modelCube.getCubeSide(CubeSideIdentifier.BOTTOM));
        rightCubeSides.add(modelCube.getCubeSide(CubeSideIdentifier.RIGHT));
        rightCubeSides.add(modelCube.getCubeSide(CubeSideIdentifier.BACK));

        //TODO Replace with Webcam input:
        leftImage = new SimpleImageUI(getClass().getResourceAsStream("testimages/testImage_000_front.png"));
        rightImage = new SimpleImageUI(getClass().getResourceAsStream("testimages/testImage_000_back.png"));
        //////////////////////////////////

        CubeUI cubeUi = new CubeUI(30);

        leftImageController = new CubeImageController(modelCube, leftCubeSides, leftImageView,
                leftImageContainer, leftMarkerPane, cubeUi, leftImage);
        leftImageController.setProbeStatusLabel(probeStatus);

        rightImageController = new CubeImageController(modelCube, rightCubeSides, rightImageView,
                rightImageContainer, rightMarkerPane, cubeUi, rightImage);
        rightImageController.setProbeStatusLabel(probeStatus);

        recognizedCubeImage.getChildren().add(cubeUi);

        modelCube.getCubeSides().forEach(cubeSide -> {

            List<XY> config = ProbeConfig.getConfig(cubeSide.getId());

            for (int i = 0; i < 9; i++) {
                Facelet facelet = cubeSide.getFacelet(i);

                facelet.setImageCoordinates(new XY(config.get(i).getX(), config.get(i).getY()));
            }
        });
    }

    /**
     * Delegates mouse clicks to the respective {@link CubeImageController}.
     *
     * @param event The mouse event to get positions from.
     */
    public void recordProbePoint(MouseEvent event) {
        if (event.getSource().equals(leftMarkerPane)) {
            leftImageController.recordProbePoint(event);
        } else if (event.getSource().equals(rightMarkerPane)) {
            rightImageController.recordProbePoint(event);
        }
    }

    /**
     * onClick event handler which prints out facelet colors.
     */
    public void recognizeAndPrintToConsole() {
        modelCube.printToConsole(leftImage, rightImage);
    }

    /**
     * onClick event handler which reads out colors and saves them to the model.
     */
    public void recognizeAndFillUi() {
        try {
            modelCube.updateModel(leftImage, rightImage);
        } catch (CouldNotDetermineSideColorsException e) {
            couldNotDetermineSideColorsPopup();
        }
    }

    /**
     * onClick event handler which prints the cube string to stdout.
     */
    public void printCubeStringToConsole() {
        try {
            System.out.println("Kociemba's cube representation: " + modelCube.getKociembaCubeString());
        } catch (ModelNeverUpdatedException e) {
            if (modelNotUpdatedPopup()) {
                printCubeStringToConsole();
            }
        }
    }

    /**
     * onClick event handler which prints the solve string to console using Herbert Kociemba's two phase algorithm.
     */
    public void printSolveStringToConsole() {
        try {
            System.out.println(modelCube.getSolution(5000).getSolutionString());
        } catch (ModelNeverUpdatedException e) {
            if (modelNotUpdatedPopup()) {
                printSolveStringToConsole();
            }
        } catch (CubeNotSolvableException e) {
            cubeNotSolvablePopup(e);
        }
    }

    /**
     * onClick event handler which prints the solution moves to console.
     */
    public void printCubeMovesToConsole() {
        try {
            LinkedList<CubeMove> cubeMoves = modelCube.getSolution(5000).getCubeMoves();
            for (int i = 0; i < cubeMoves.size(); i++) {
                CubeMove move = cubeMoves.get(i);
                System.out.println("Step: " + (i + 1) + "  \t" + move.getCubeSide().name() + ":\t" + move.getDirection().name());
            }
        } catch (ModelNeverUpdatedException e) {
            if (modelNotUpdatedPopup()) {
                printCubeMovesToConsole();
            }
        } catch (CubeNotSolvableException e) {
            cubeNotSolvablePopup(e);
        }
    }

    /**
     * onClick event handler which starts configuration mode for probe points on the respective {@link CubeImageController}.
     *
     * @param event The onClick event.
     */
    public void configureProbePoints(Event event) {
        if (event.getSource().equals(probeTop)) {
            leftImageController.setProbeConfigureMode(CubeSideIdentifier.TOP);
        } else if (event.getSource().equals(probeBottom)) {
            rightImageController.setProbeConfigureMode(CubeSideIdentifier.BOTTOM);
        } else if (event.getSource().equals(probeLeft)) {
            leftImageController.setProbeConfigureMode(CubeSideIdentifier.LEFT);
        } else if (event.getSource().equals(probeRight)) {
            rightImageController.setProbeConfigureMode(CubeSideIdentifier.RIGHT);
        } else if (event.getSource().equals(probeFront)) {
            leftImageController.setProbeConfigureMode(CubeSideIdentifier.FRONT);
        } else if (event.getSource().equals(probeBack)) {
            rightImageController.setProbeConfigureMode(CubeSideIdentifier.BACK);
        }
    }

    /**
     * onClick event handler which saves JSON files.
     *
     * @param event The onClick event
     */
    public void saveJson(Event event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON files", "*.json"));

        File file = fileChooser.showSaveDialog(primaryStage);

        if (file != null) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.INDENT_OUTPUT, true);

            Object source = null;

            if (event.getSource() == saveProbePoints) {
                source = new ProbePointConfig(modelCube);
            }

            try {
                mapper.writeValue(file, source);
            } catch (Exception e) {
                Alert errorPopup = new Alert(Alert.AlertType.ERROR);
                errorPopup.setTitle("Could not save JSON");
                errorPopup.setHeaderText("When trying to save the desired file, an error occured.");
                errorPopup.setContentText("This exception was thrown:\n\n"
                        + e.getClass().getName() + ": " + e.getMessage());
                errorPopup.showAndWait();
            }
        }
    }

    /**
     * onClick handler which opens JSON files.
     *
     * @param event The onClick event.
     */
    public void openJson(Event event) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("JSON files", "*.json"));

        File file = fileChooser.showOpenDialog(primaryStage);

        if(file != null) {
            ObjectMapper mapper = new ObjectMapper();

            try {
                if (event.getSource() == openProbePoints) {
                    mapper.readValue(file, ProbePointConfig.class).apply(modelCube);
                }
            } catch (Exception e) {
                Alert errorPopup = new Alert(Alert.AlertType.ERROR);
                errorPopup.setTitle("Could not load JSON");
                errorPopup.setHeaderText("When trying to load the desired file, an error occured.");
                errorPopup.setContentText("This exception was thrown:\n\n"
                        + e.getClass().getName() + ": " + e.getMessage());
                errorPopup.showAndWait();
            }
        }
    }

    /**
     * Displays error popup when cube is not solvable.
     *
     * @param exception The {@link CubeNotSolvableException} which is thrown to determine the error.
     */
    private void cubeNotSolvablePopup(CubeNotSolvableException exception) {
        Alert errorPopup = new Alert(Alert.AlertType.ERROR);
        errorPopup.setTitle("Cube not solvable");
        errorPopup.setHeaderText("The recognized cube is not solvable!");
        errorPopup.setContentText("The algorithm returned this error message:\n\n" + exception.getMessage());
        errorPopup.showAndWait();
    }

    /**
     * Displays error popup when the sides' primary colors could not be determined.
     */
    private void couldNotDetermineSideColorsPopup() {
        Alert errorPopup = new Alert(Alert.AlertType.ERROR);
        errorPopup.setTitle("Could not determine side colors");
        errorPopup.setHeaderText("The cube sides' primary colors could not be determined!");
        errorPopup.setContentText("This could result from misarranged probe points or multiple center points getting " +
                "the same color.");
        errorPopup.showAndWait();
    }

    /**
     * Displays error popup when ProbeConfig is not valid.
     */
    private void probeConfigNotReadyPopup() {
        Alert errorPopup = new Alert(Alert.AlertType.ERROR);
        errorPopup.setTitle("Probe config is not valid");
        errorPopup.setHeaderText("Probes are not configured properly");
        errorPopup.setContentText("Configure pixel probes correctly first.");
        errorPopup.showAndWait();
    }

    /**
     * Displays error popup when model is not updated but needs to be.
     *
     * @return true if user clicked "Update model", false if "Cancel"
     */
    private boolean modelNotUpdatedPopup() {
        ButtonType updateButton = new ButtonType("Update model");
        ButtonType cancelButton = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        Alert errorPopup = new Alert(Alert.AlertType.ERROR);
        errorPopup.setTitle("Model not updated");
        errorPopup.setHeaderText("You need to update the model first.");
        errorPopup.setContentText("It looks like the model was not updated at this point. To get a valid " +
                "cube string, this needs to be done first.\nShould we do that now?");
        errorPopup.getButtonTypes().setAll(updateButton, cancelButton);

        if (errorPopup.showAndWait().get() == updateButton) {
            recognizeAndFillUi();
            return true;
        } else {
            return false;
        }
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
}

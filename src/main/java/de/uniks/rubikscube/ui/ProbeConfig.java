package de.uniks.rubikscube.ui;

import de.uniks.rubikscube.model.CubeSideIdentifier;
import de.uniks.rubikscube.model.XY;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Currently stores default probe point config. Will be replaced soon.
 */
public class ProbeConfig {

    // TODO Make nice

    private static HashMap<CubeSideIdentifier, List<XY>> probeConfigs;

    static {

        probeConfigs = new HashMap<>();

        probeConfigs.put(CubeSideIdentifier.TOP, Arrays.asList(
                new XY(75, 65),
                new XY(150, 53),
                new XY(213, 36),
                new XY(117, 93),
                new XY(194, 75),
                new XY(250, 58),
                new XY(159, 123),
                new XY(242, 103),
                new XY(312, 82)
        ));

        probeConfigs.put(CubeSideIdentifier.BOTTOM, Arrays.asList(
                new XY(213, 363),
                new XY(150, 349),
                new XY(75, 333),
                new XY(255, 341),
                new XY(188, 325),
                new XY(113, 306),
                new XY(311, 318),
                new XY(243, 298),
                new XY(163, 277)
        ));

        probeConfigs.put(CubeSideIdentifier.LEFT, Arrays.asList(
                new XY(44, 116),
                new XY(80, 145),
                new XY(125, 173),
                new XY(50, 185),
                new XY(85, 219),
                new XY(129, 256),
                new XY(57, 251),
                new XY(93, 286),
                new XY(131, 330)
        ));

        probeConfigs.put(CubeSideIdentifier.RIGHT, Arrays.asList(
                new XY(60, 149),
                new XY(92, 114),
                new XY(134, 72),
                new XY(48, 211),
                new XY(89, 180),
                new XY(132, 143),
                new XY(43, 286),
                new XY(83, 258),
                new XY(129, 223)
        ));

        probeConfigs.put(CubeSideIdentifier.FRONT, Arrays.asList(
                new XY(186, 185),
                new XY(269, 158),
                new XY(335, 136),
                new XY(191, 264),
                new XY(263, 240),
                new XY(327, 211),
                new XY(187, 338),
                new XY(262, 305),
                new XY(323, 281)
        ));

        probeConfigs.put(CubeSideIdentifier.BACK, Arrays.asList(
                new XY(192, 66),
                new XY(257, 93),
                new XY(323, 118),
                new XY(191, 138),
                new XY(265, 164),
                new XY(328, 187),
                new XY(188, 214),
                new XY(267, 242),
                new XY(335, 263)
        ));
    }

    public static List<XY> getConfig(CubeSideIdentifier id) {
        return probeConfigs.get(id);
    }
}

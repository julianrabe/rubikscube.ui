package de.uniks.rubikscube.ui;

import de.uniks.rubikscube.model.XY;
import javafx.scene.Group;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Line;

/**
 * Used to mark probe points on cube's UI representations.
 */
public class ProbeMarker extends Group {

    private final Line line1;
    private final Line line2;
    private XY coordinates;

    ProbeMarker(XY coordinates, Paint color) {
        this.coordinates = coordinates;

        line1 = new Line(coordinates.getX() - 2, coordinates.getY() - 2,
                coordinates.getX() + 2, coordinates.getY() + 2);
        line1.setStroke(color);

        line2 = new Line(coordinates.getX() - 2, coordinates.getY() + 2,
                coordinates.getX() + 2, coordinates.getY() - 2);
        line2.setStroke(color);

        getChildren().addAll(line1, line2);
    }

    /**
     * Updates internal elements according to scale. Used for window resizing e.g.
     *
     * @param scale The scale to use, calculates using {@link CubeImageController#computeScaleFactor()}.
     */
    public void redrawWithScale(double scale) {

        line1.setStartX(coordinates.getX() * scale - 2);
        line1.setStartY(coordinates.getY() * scale - 2);
        line1.setEndX(coordinates.getX() * scale + 2);
        line1.setEndY(coordinates.getY() * scale + 2);

        line2.setStartX(coordinates.getX() * scale - 2);
        line2.setStartY(coordinates.getY() * scale + 2);
        line2.setEndX(coordinates.getX() * scale + 2);
        line2.setEndY(coordinates.getY() * scale - 2);
    }

    public XY getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(XY coordinates) {
        this.coordinates = coordinates;
    }
}

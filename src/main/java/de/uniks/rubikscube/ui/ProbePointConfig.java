package de.uniks.rubikscube.ui;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import de.uniks.rubikscube.model.Cube;
import de.uniks.rubikscube.model.CubeSide;
import de.uniks.rubikscube.model.CubeSideIdentifier;
import de.uniks.rubikscube.model.XY;

import java.util.HashMap;
import java.util.Map;

class ProbePointConfig {

    private Map<CubeSideIdentifier, XY[]> sideMap;

    @JsonCreator
    private ProbePointConfig(@JsonProperty("sideMap") Map<CubeSideIdentifier, XY[]> sideMap) {
        this.sideMap = sideMap;
    }

    ProbePointConfig(Cube cube) {
        sideMap = new HashMap<>();

        for (CubeSide cubeSide : cube.getCubeSides()) {
            XY[] points = new XY[9];
            for (int i = 0; i < 9; i++) {
                points[i] = cubeSide.getFacelet(i).getImageCoordinates();
            }
            sideMap.put(cubeSide.getId(), points);
        }
    }

    public Map<CubeSideIdentifier, XY[]> getSideMap() {
        return sideMap;
    }

    public void apply(Cube modelCube) {
        sideMap.forEach(((cubeSideIdentifier, coordinates) -> {
            CubeSide cubeSide = modelCube.getCubeSide(cubeSideIdentifier);
            for (int i = 0; i < 9; i++) {
                cubeSide.getFacelet(i).setImageCoordinates(new XY(coordinates[i].getX(), coordinates[i].getY()));
            }
        }));
    }
}

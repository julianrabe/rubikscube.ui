package de.uniks.rubikscube.ui;

import de.uniks.rubikscube.model.SimpleImage;
import javafx.scene.image.Image;

import java.io.InputStream;

public class SimpleImageUI extends Image implements SimpleImage {

    SimpleImageUI(InputStream is) {
        super(is);
    }

    @Override
    public String getColor(int x, int y) {
        return this.getPixelReader().getColor(x, y).toString();
    }
}
